package task.firstSymbolCheck.module;


import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.*;

import lombok.*;

import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor

@Entity(name = "texts_and_results")
public class Text {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(length = 1000)
    @JsonProperty("text")
    private String text;

    @JsonProperty("result")
    private Character result;
}
