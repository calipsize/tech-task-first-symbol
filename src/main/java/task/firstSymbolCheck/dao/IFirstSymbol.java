package task.firstSymbolCheck.dao;


import task.firstSymbolCheck.module.Text;

import java.util.List;
import java.util.UUID;


public interface IFirstSymbol {
    Text getResults(UUID token);

    List<Text> getAllResults();

    Text saveResults(Text text);
}
