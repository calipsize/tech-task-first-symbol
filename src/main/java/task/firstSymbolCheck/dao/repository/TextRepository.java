package task.firstSymbolCheck.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import task.firstSymbolCheck.module.Text;
import java.util.UUID;


public interface TextRepository extends JpaRepository<Text, UUID> {

}
