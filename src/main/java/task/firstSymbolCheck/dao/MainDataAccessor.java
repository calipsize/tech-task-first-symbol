package task.firstSymbolCheck.dao;


import io.micrometer.common.util.StringUtils;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import task.firstSymbolCheck.dao.repository.TextRepository;
import task.firstSymbolCheck.module.Text;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Repository("MainAccessor")
public class MainDataAccessor implements IFirstSymbol {
    private final TextRepository textRepository;

    @Autowired
    public MainDataAccessor(TextRepository textRepository) {
        this.textRepository = textRepository;
    }


    @Override
    public Text getResults(UUID token) {
        if (StringUtils.isBlank(token.toString()))
            throw new IllegalArgumentException("The illegal format of data!");

        return textRepository.findById(token).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<Text> getAllResults() {
        List<Text> tempTextStorage = textRepository.findAll();

        if (tempTextStorage.isEmpty()) throw new EntityNotFoundException("The list is empty!");

        return tempTextStorage;
    }


    @Override
    public Text saveResults(Text text) {
        if (StringUtils.isBlank(text.getText()))
            throw new IllegalArgumentException("The illegal format of data!");

        String[] tempTextHolder = text.getText().split(" ");

        Map<Character, Long> charCountMap = Arrays.stream(tempTextHolder)
                .filter(s -> !s.isEmpty())
                .map(s -> s.charAt(0))
                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));

        Character result = charCountMap.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No non-repeating characters found!"));

        text.setResult(result);

        return textRepository.save(text);
    }
}
