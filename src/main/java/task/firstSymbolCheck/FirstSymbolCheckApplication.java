package task.firstSymbolCheck;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FirstSymbolCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstSymbolCheckApplication.class, args);
	}

}
