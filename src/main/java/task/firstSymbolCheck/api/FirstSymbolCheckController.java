package task.firstSymbolCheck.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import task.firstSymbolCheck.module.Text;
import task.firstSymbolCheck.service.FirstSymbolCheckService;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/first-symbol")
public class FirstSymbolCheckController {
    private final FirstSymbolCheckService firstSymbolCheckService;

    @Autowired
    public FirstSymbolCheckController(FirstSymbolCheckService firstSymbolCheckService) {
        this.firstSymbolCheckService = firstSymbolCheckService;
    }

    @GetMapping("/getResults/{token}")
    public ResponseEntity<Text> getResults(@PathVariable("token") UUID token) {
        return ResponseEntity.ok().body(firstSymbolCheckService.getResultsService(token));
    }

    @GetMapping("/getAllResults")
    public ResponseEntity<List<Text>> getAllResults() {
        return ResponseEntity.ok().body(firstSymbolCheckService.getAllResultsService());
    }

    @PostMapping("/saveText")
    public ResponseEntity<Text> saveText(@RequestBody Text text) {
        return ResponseEntity.ok().body(firstSymbolCheckService.saveResultsService(text));
    }
}
