package task.firstSymbolCheck.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import task.firstSymbolCheck.dao.IFirstSymbol;
import task.firstSymbolCheck.module.Text;

import java.util.List;
import java.util.UUID;


@Service
public class FirstSymbolCheckService {
    private final IFirstSymbol iFirstSymbol;

    @Autowired
    public FirstSymbolCheckService(@Qualifier("MainAccessor") IFirstSymbol iFirstSymbol) {
        this.iFirstSymbol = iFirstSymbol;
    }

    @Transactional(readOnly = true)
    public Text getResultsService(UUID token) {
        return iFirstSymbol.getResults(token);
    }

    @Transactional(readOnly = true)
    public List<Text> getAllResultsService() {
        return iFirstSymbol.getAllResults();
    }

    @Transactional
    public Text saveResultsService(Text text) {
        return iFirstSymbol.saveResults(text);
    }
}
