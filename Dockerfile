FROM openjdk:17-jdk-alpine
ARG JAR_FILE=./*.jar
COPY ./firstSymbolCheck.jar first-symbol-check-api.jar
CMD ["java", "-jar", "first-symbol-check-api.jar"]
